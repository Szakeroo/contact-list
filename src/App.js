import './App.scss';
import { ContactList } from "./components/ContactList"
import { createMuiTheme } from '@material-ui/core';

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ffffff'
    },
  }
})
function App() {
  return (
    <ContactList />
  );
}

export default App;
