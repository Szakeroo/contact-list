import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();

const filterData = (array,param) => {
  const resultArray = []
  array.forEach((user) => {
      const name = user.first_name.toLowerCase();
      const surname = user.last_name.toLowerCase();
      if(name.includes(param.toLowerCase()) | surname.includes(param.toLowerCase)){
          resultArray.push(user)
      } 
      return Set(resultArray)
  })
  
}