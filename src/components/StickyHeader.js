import AppBar from '@material-ui/core/AppBar';
import { Header } from "./Header";
import { SearchBar } from './SearchBar';

export const StickyHeader = () => {
    return (
        <AppBar position="sticky" id="sticky__container">
            <Header name="Contact List" />
            <SearchBar />
        </AppBar>
    )
}