import React, { useEffect, useState, useContext, createContext } from "react";
import { StickyHeader } from "./StickyHeader";
import { UserCell } from "./UserCell";
import CircularProgress from '@material-ui/core/CircularProgress'
export const UsersContext = createContext();
export const CheckedContext = createContext();
export const ContactList = () => {
    const APILINK = "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
    const [users, setUsers] = useState([]);
    const [checkedUsers, setCheckedUsers] = [];
    const [filteredUsers, setFilteredUsers] = useState([]);

    const sortDataByLastName = (object1, object2) => {
        const firstLastName = object1.last_name.toLowerCase();
        const secondLastName = object2.last_name.toLowerCase();
        let compared = 0;
        if (firstLastName > secondLastName) {
            compared = 1;
        } else if (firstLastName < secondLastName) {
            compared = -1;
        }
        return compared;
    }
    const handleGetUsers = () => {
        fetch(`${APILINK}`)
            .then(async response => {
                try {
                    const data = await response.json();
                    setUsers(data.sort(sortDataByLastName));
                    setFilteredUsers(data.sort(sortDataByLastName));
                } catch (error) {
                    alert(error);
                }
            })
    };
    useEffect(() => {
        handleGetUsers();
    }, []);

    return (
        <>
            <UsersContext.Provider value={{ users, setUsers,filteredUsers, setFilteredUsers }}>
                <StickyHeader />
                {users.length === 0 ? <div className="loading">
                    Loading or Input Out of Range
                    <br /><CircularProgress id="loading__icon" /> </div> : <ul>
                    {users.map((userObject) =>
                        <UserCell key={userObject.id} id={userObject.id} firstName={userObject.first_name} lastName={userObject.last_name}
                            gender={userObject.gender} email={userObject.email}
                            avatar={userObject.avatar} />)}
                </ul>
                }
            </UsersContext.Provider>
        </>
    )
}