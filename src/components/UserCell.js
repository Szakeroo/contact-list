import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { DataCell } from "./DataCell";

export const UserCell = ({ firstName, lastName, avatar, id }) => {
    return (
        <div className="user__container">
            <Avatar
                alt={`${firstName} ${lastName}`}
                src={avatar === null ? "./assets/placeholder.png" : avatar}
            />
            <DataCell firstName={firstName} lastName={lastName} id={id} />
        </div>
    );
};
