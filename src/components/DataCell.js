import React, { useState } from "react"
import Checkbox from '@material-ui/core/Checkbox';
export const DataCell = ({ firstName, lastName, id }) => {
  return (
    <>
      <p className="data__cell">{firstName} {lastName}</p>
      <Checkbox id={id} key={id} inputProps={{ 'aria-label': 'secondary checkbox' }} />
    </>

  )
}