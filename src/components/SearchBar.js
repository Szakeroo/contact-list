import React, { useContext, useEffect, useState } from "react"
import { ThemeProvider } from '@material-ui/styles';
import { theme } from "../App";
import TextField from '@material-ui/core/TextField';
import { UsersContext } from "./ContactList";
export const SearchBar = () => {
    const [searchParam, setSearchParam] = useState("");
    const { users, setUsers,filteredUsers, setFilteredUsers } = useContext(UsersContext);

    const handleSearchInput = (event) => {
        setSearchParam(event.target.value);
    }
    const filterData = (array, param) => {
        const resultArray = []
        array.forEach((user) => {
            const userData = user.first_name + " " + user.last_name.split()
            if (userData.includes(param)) {
                resultArray.push(user);
            }
        })
        return resultArray;
    }
    useEffect(() => {
        setUsers(filterData(filteredUsers, searchParam))
    }, [searchParam]);

    return (
        <ThemeProvider theme={theme}>
            <TextField onChange={handleSearchInput} label="Search..." variant="outlined" color="primary" style={{ marginRight: '5em' }} />
        </ThemeProvider>
    )
}
